﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using SimpleJson;
using Swind;

namespace DocumentPackerTest
{
    [TestClass]
    public class CDFJsonSerializerTest
    {
        private CDFJsonSerializer serializer;

        [TestInitialize]
        public void Initialize()
        {
            serializer = new CDFJsonSerializer();
        }

        [TestMethod]
        public void SerializeTest()
        {
            List<string> typeRowList = new List<string>();
            List<string> headerRowList = new List<string>();
            List<List<string>> tableContentList = new List<List<string>>();

            typeRowList.Add("string");
            typeRowList.Add("string");
            typeRowList.Add("int");
            typeRowList.Add("comment");

            headerRowList.Add("Name");
            headerRowList.Add("Profession");
            headerRowList.Add("Age");
            headerRowList.Add("Description");

            List<string> contentRowList = new List<string>();
            contentRowList.Add("car");
            contentRowList.Add("IT");
            contentRowList.Add("25");
            contentRowList.Add("handsome");
            tableContentList.Add(contentRowList);

            CDFFile file = new CDFFile();
            file.typeRowList = typeRowList;
            file.headerRowList = headerRowList;
            file.tableContentList = tableContentList as List<List<string>>;

            serializer.Serialize(file,"JsonSerializerTest.cjf");
        }

        [TestMethod]
        public void DeserializeTest()
        {
            CDFJsonFile jsonFile = serializer.Deserialize("JsonSerializerTest.cdf") as CDFJsonFile;
            JsonArray typeRowJsonArray = jsonFile.typeJsonArray;
            JsonArray headerRowJsonArray = jsonFile.headerJsonArray;
            List<JsonArray> tableContentList = jsonFile.tableContentList;
        }

        [TestCleanup]
        public void Cleanup()
        {
            serializer = null;
        }
    }
}
