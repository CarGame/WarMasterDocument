﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Swind;
using System.IO;

namespace DocumentPackerTest
{
    [TestClass]
    public class DocumentExporterTest
    {
        private DocumentExporter exporter;

        [TestInitialize]
        public void Initialize()
        {
            exporter = new DocumentExporter();
        }

        [TestMethod]
        public void ExportJsonFileTest()
        {
            exporter.ExportJsonFile("Hero.xls", "Results");
        }

        [TestMethod]
        public void ExportJsonFileRecursivelyTest()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            exporter.ExportJsonFileRecursively(currentDirectory, "Results");
        }

        [TestMethod]
        public void ExportBinaryFileTest()
        {
            exporter.ExportBinaryFile("Hero.xls", "Results");
        }

        [TestMethod]
        public void ExportBinaryFileRecursivelyTest()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            exporter.ExportBinaryFileRecursively(currentDirectory, "Results");
        }

        [TestCleanup]
        public void Cleanup()
        {
            exporter = null;
        }
    }
}
