﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Swind;
using System.Collections.Generic;

namespace DocumentPackerTest
{
    [TestClass]
    public class CDFBinarySerializerTest
    {
        private CDFBinarySerializer serializer; 

        [TestInitialize]
        public void Initialize()
        {
            serializer = new CDFBinarySerializer();
        }

        [TestMethod]
        public void SerializeTest()
        {
            List<string> typeRowList = new List<string>();
            List<string> headerRowList = new List<string>();
            List<List<string>> tableContentList = new List<List<string>>();

            typeRowList.Add("string");
            typeRowList.Add("string");
            typeRowList.Add("int");
            typeRowList.Add("comment");

            headerRowList.Add("Name");
            headerRowList.Add("Profession");
            headerRowList.Add("Age");
            headerRowList.Add("Description");

            List<string> contentRowList = new List<string>();
            contentRowList.Add("car");
            contentRowList.Add("IT");
            contentRowList.Add("25");
            contentRowList.Add("handsome");
            tableContentList.Add(contentRowList);

            CDFFile file = new CDFFile();
            file.typeRowList = typeRowList;
            file.headerRowList = headerRowList;
            file.tableContentList = tableContentList as List<List<string>>;

            serializer.Serialize(file, "JsonSerializerTest.cbf");
        }

        [TestMethod]
        public void DeserializeTest()
        {
            CDFFile cdfFile = serializer.Deserialize("JsonSerializerTest.cbf") as CDFFile;
            List<string> typeRowList = cdfFile.typeRowList;
            List<string> headerRowList = cdfFile.headerRowList;
            List<List<string>> tableContentList = cdfFile.tableContentList;
        }

        [TestCleanup]
        public void Cleanup()
        {
            serializer = null;
        }

    }
}
