﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Swind;
using System.Collections.Generic;

namespace DocumentPackerTest
{
    [TestClass]
    public class CDFTableReaderTest
    {
        private CDFTableReader reader;

        [TestInitialize]
        public void Initialize()
        {
            reader = new CDFTableReader();
        }

        [TestMethod]
        public void CDFTableReaderReadFileTest()
        {
            reader.Initialize("Hero.xls");

            List<string> typeRowList = reader.GetTableHeaderRowList(0, CDFTableReader.TableHeaderSection.typeRow);
            List<string> headerRowList = reader.GetTableHeaderRowList(0, CDFTableReader.TableHeaderSection.headerRow);
            List<List<string>> tableContentList = reader.GetTableContentList(0);

            int sheetCount = reader.GetSheetCount();

        }

        [TestCleanup]
        public void Cleanup()
        {
            reader = null;
        }

    }
}
