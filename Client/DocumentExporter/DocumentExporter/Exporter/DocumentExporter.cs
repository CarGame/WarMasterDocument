﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Swind
{
    public class DocumentExporter : Singleton<DocumentExporter>
    {
        private void ExportCDFFile(string sourcePathFilename, string targetPath, CDFSerializer serializer,string targetExtensionName)
        {
            string sourceFileName = Path.GetFileNameWithoutExtension(sourcePathFilename);
            string targetFullPathName = string.Empty;

            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }

            CDFFile cdfFile = new CDFFile();
            CDFTableReader cdfTableReader = new CDFTableReader();
            bool result = cdfTableReader.Initialize(sourcePathFilename);
            if (!result)
            {
                return;
            }

            int sheetCount = cdfTableReader.GetSheetCount();
            for (int i = 0; i < sheetCount; ++i)
            {
                string sheetName = cdfTableReader.GetSheetName(i);
                targetFullPathName = string.Concat(targetPath, "\\", sheetName, targetExtensionName);

                List<string> typeRowList = cdfTableReader.GetTableHeaderRowList(i, CDFTableReader.TableHeaderSection.typeRow);
                if (typeRowList != null)
                {
                    cdfFile.typeRowList = typeRowList;
                }
                else
                {
                    Console.WriteLine(string.Format(StringConfigFile.SheetInvalidMsg, sourceFileName));
                    continue;
                }

                List<string> headerRowList = cdfTableReader.GetTableHeaderRowList(i, CDFTableReader.TableHeaderSection.headerRow);
                if (headerRowList != null)
                {
                    cdfFile.headerRowList = headerRowList;
                }
                else
                {
                    Console.WriteLine(string.Format(StringConfigFile.SheetInvalidMsg, sourceFileName));
                    continue;
                }

                List<List<string>> tableContentList = cdfTableReader.GetTableContentList(i);
                if (tableContentList != null)
                {
                    cdfFile.tableContentList = tableContentList;
                }
                else
                {
                    Console.WriteLine(string.Format(StringConfigFile.SheetInvalidMsg, sourceFileName));
                    continue;
                }

                serializer.Serialize(cdfFile, targetFullPathName);
            }
        }

        public void ExportJsonFile(string sourcePathFilename, string targetPath)
        {
            CDFJsonSerializer serializer = new CDFJsonSerializer();
            ExportCDFFile(sourcePathFilename, targetPath, serializer,StringConfigFile.CDFJsonFileExtensionName);
        }

        public void ExportJsonFileRecursively(string sourcePath, string targetPath)
        {
            string[] paths = Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories);

            for (int i = 0; i < paths.Length; ++i)
            {
                int lastSourceIndex = paths[i].LastIndexOf(StringConfigFile.SourceDirectoryName);
                string relativeSourcePath = paths[i].Substring(lastSourceIndex);
                
                string sourceDirectoryPath = Path.GetDirectoryName(paths[i]);
                lastSourceIndex = sourceDirectoryPath.LastIndexOf(StringConfigFile.SourceDirectoryName);
                string relativeSourceDirectoryName = sourceDirectoryPath.Substring(lastSourceIndex);
                string relativeTargetPath = relativeSourceDirectoryName.Replace(StringConfigFile.SourceDirectoryName, targetPath);

                ExportJsonFile(relativeSourcePath, relativeTargetPath);
            }
        }

        public void ExportBinaryFile(string sourcePathFilename, string targetPath)
        {
            CDFBinarySerializer serializer = new CDFBinarySerializer();
            ExportCDFFile(sourcePathFilename, targetPath, serializer,StringConfigFile.CDFBinaryFileExtensionName);
        }

        public void ExportBinaryFileRecursively(string sourcePath, string targetPath)
        {
            string[] paths = Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories);

            for (int i = 0; i < paths.Length; ++i)
            {
                ExportBinaryFile(paths[i], targetPath);
            }
        }

    }
}
