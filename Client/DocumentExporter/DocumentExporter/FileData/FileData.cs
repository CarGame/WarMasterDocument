﻿using System;
using System.Collections.Generic;
using SimpleJson;

namespace Swind
{
    [Serializable]
    public class CDFFile
    {
        public List<string> typeRowList;
        public List<string> headerRowList;
        public List<List<string>> tableContentList;
    }

    public class CDFJsonFile
    {
        public JsonArray typeJsonArray;
        public JsonArray headerJsonArray;
        public List<JsonArray> tableContentList;
    }

    public class CDFFileToken
    {
        public static readonly string StringToken = "string";
        public static readonly string IntToken = "int";
        public static readonly string FloatToken = "float";
        public static readonly string CommentToken = "comment";
    }
}
