﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Swind
{
    class Program
    {
        static void Main(string[] args)
        {
            string sourcePath = Directory.GetCurrentDirectory();
            sourcePath = Path.Combine(sourcePath, StringConfigFile.SourceDirectoryName);
            string targetPath = StringConfigFile.TargetDirectoryName;

            if (Directory.Exists(sourcePath))
            {
                DocumentExporter exporter = DocumentExporter.Instance;
                exporter.ExportJsonFileRecursively(sourcePath, targetPath);
            }
            else
            {
                Console.WriteLine(StringConfigFile.SourceDirectoryError);
            }

            Console.WriteLine("press any key to exit...");
            Console.Read();
        }
    }
}
