﻿namespace Swind
{
    public abstract class CDFSerializer
    {
        public abstract void Serialize(object graph, string targetPath);
        public abstract object Deserialize(string southPath);
    }

}
