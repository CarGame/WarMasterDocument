﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Swind
{
    public class CDFBinarySerializer : CDFSerializer
    {
        public override void Serialize(object graph, string targetPath)
        {
            CDFFile cdfFile = graph as CDFFile;
            cdfFile = CDFTools.TrimDataFileContent(cdfFile);
            cdfFile = CDFTools.RemoveCommentColumn(cdfFile);
            cdfFile = CDFTools.RemoveEmptyColumn(cdfFile);

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(targetPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            formatter.Serialize(stream, cdfFile);
            stream.Close();
        }

        public override object Deserialize(string southPath)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(southPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            object graph = formatter.Deserialize(stream);
            stream.Close();

            return graph;
        }
    }

}
