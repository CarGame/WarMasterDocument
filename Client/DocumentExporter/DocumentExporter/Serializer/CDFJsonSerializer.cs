﻿using SimpleJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Swind
{
    public class CDFJsonSerializer : CDFSerializer
    {
        public override void Serialize(object graph, string targetPath)
        {
            FileStream fileStream = null;
            StreamWriter writer = null;

            CDFFile cdfFile = graph as CDFFile;
            cdfFile = CDFTools.TrimDataFileContent(cdfFile);
            cdfFile = CDFTools.RemoveCommentColumn(cdfFile);
            cdfFile = CDFTools.RemoveEmptyColumn(cdfFile);

            try
            {
                fileStream = File.Open(targetPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                writer = new StreamWriter(fileStream, new UTF8Encoding(true));

                Console.WriteLine(string.Format(StringConfigFile.SerializingMsg, targetPath));

                string typeJson = SimpleJson.SimpleJson.SerializeObject(cdfFile.typeRowList);
                writer.WriteLine(typeJson);
                Console.WriteLine(typeJson);

                string headerJson = SimpleJson.SimpleJson.SerializeObject(cdfFile.headerRowList);
                writer.WriteLine(headerJson);
                Console.WriteLine(headerJson);

                for (int i = 0; i < cdfFile.tableContentList.Count; ++i)
                {
                    string tableContentRowJson = SimpleJson.SimpleJson.SerializeObject(cdfFile.tableContentList[i]);

                    if (i != cdfFile.tableContentList.Count - 1)
                    {
                        writer.WriteLine(tableContentRowJson);
                    }
                    else
                    {
                        writer.Write(tableContentRowJson);
                    }

                    Console.WriteLine(tableContentRowJson);
                }

                Console.WriteLine();

                writer.Close();
                fileStream.Close();
            }
            catch (IOException exception)
            {
                writer.Close();
                fileStream.Close();
                Console.WriteLine(exception.Message);
            }
            catch (Exception exception)
            {
                writer.Close();
                fileStream.Close();
                Console.WriteLine(exception.Message);
            }
        }

        public override object Deserialize(string southPath)
        {
            FileStream fileStream = null;
            StreamReader reader = null;
            CDFJsonFile graph = new CDFJsonFile();

            try
            {
                fileStream = File.Open(southPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                reader = new StreamReader(fileStream);

                string typeRowJson = reader.ReadLine();
                graph.typeJsonArray = SimpleJson.SimpleJson.DeserializeObject(typeRowJson) as JsonArray;

                string headerRowJson = reader.ReadLine();
                graph.headerJsonArray = SimpleJson.SimpleJson.DeserializeObject(headerRowJson) as JsonArray;

                graph.tableContentList = new List<JsonArray>();
                while (!reader.EndOfStream)
                {
                    string contentRowJson = reader.ReadLine();
                    object testObject = SimpleJson.SimpleJson.DeserializeObject(contentRowJson);
                    JsonArray contentRowJsonArray = SimpleJson.SimpleJson.DeserializeObject(contentRowJson) as JsonArray;
                    graph.tableContentList.Add(contentRowJsonArray);
                }

                fileStream.Close();
                return graph;
            }
            catch (IOException exception)
            {
                fileStream.Close();
                Console.WriteLine(exception.Message);
            }
            catch (Exception exception)
            {
                fileStream.Close();
                Console.WriteLine(exception.Message);
            }

            return graph;
        }
    }
}
