﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swind
{
    public class CDFTools
    {
        public static CDFFile RemoveCommentColumn(CDFFile file)
        {
            List<int> commentColumnIndexList = new List<int>();
            int columnCount = file.typeRowList.Count;
            for (int i = 0; i < columnCount; ++i)
            {
                if (file.typeRowList[i] == CDFFileToken.CommentToken)
                {
                    commentColumnIndexList.Add(i);
                }
            }

            int commentColumnCount = commentColumnIndexList.Count;
            for (int i = commentColumnCount - 1; i >= 0; --i)
            {
                RemoveCDFFileColumn(file, commentColumnIndexList[i]);
            }

            return file;
        }

        private static void RemoveCDFFileColumn(CDFFile file, int columnIndex)
        {
            file.typeRowList.RemoveAt(columnIndex);
            file.headerRowList.RemoveAt(columnIndex);

            for (int i = 0; i < file.tableContentList.Count; ++i)
            {
                if (file.tableContentList[i].Count - 1 >= columnIndex)
                {
                    file.tableContentList[i].RemoveAt(columnIndex);
                }
            }
        }

        public static CDFFile RemoveEmptyColumn(CDFFile file)
        {
            if (file != null)
            {
                if (ValidateDataFile(file))
                {
                    int typeCount = file.typeRowList.Count;
                    for (int i = typeCount - 1; i >= 0; --i)
                    {
                        if (string.IsNullOrEmpty(file.typeRowList[i]))
                        {
                            RemoveCDFFileColumn(file, i);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(file.typeRowList[i].Trim()))
                            {
                                RemoveCDFFileColumn(file, i);
                            }
                        }
                    }
                }
            }

            return file;
        }

        public static bool ValidateDataFile(CDFFile file)
        {
            if (file != null)
            {
                int typeCount = file.typeRowList.Count;
                int headerCount = file.headerRowList.Count;
                if (typeCount != headerCount)
                {
                    return false;
                }

                int rowCount = file.tableContentList.Count;
                for (int i = 0; i < rowCount; ++i)
                {
                    if (file.tableContentList[i] != null && file.tableContentList[i].Count != typeCount)
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public static CDFFile TrimDataFileContent(CDFFile file)
        {
            if (file != null)
            {
                int typeCount = file.typeRowList.Count;
                int headerCount = file.headerRowList.Count;
                if (headerCount > typeCount)
                {
                    int overrangeCount = headerCount - typeCount;
                    file.headerRowList.RemoveRange(typeCount, overrangeCount);
                }

                int rowCount = file.tableContentList.Count;
                for (int i = 0; i < rowCount; ++i)
                {
                    int contentRowColumnCount = file.tableContentList[i].Count;
                    if (file.tableContentList[i] != null && contentRowColumnCount > typeCount)
                    {
                        int overrangeCount = contentRowColumnCount - typeCount;
                        file.tableContentList[i].RemoveRange(typeCount, overrangeCount);
                    }
                }
            }

            return file;
        }
    }
}
