﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Swind
{
    public class StringConfigFile
    {
        public static readonly string CDFJsonFileExtensionName = ".txt";
        public static readonly string CDFBinaryFileExtensionName = ".txt";
        public static readonly string SheetInvalidMsg = "{0} is Invalid";
        public static readonly string SerializingMsg = "serializing {0}";
        public static readonly string SourceDirectoryName = "Source";
        public static readonly string TargetDirectoryName = "Target";
        public static readonly string SourceDirectoryError = "The source directory does not exist";
    }
}
