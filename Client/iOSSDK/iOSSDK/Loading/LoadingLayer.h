//
//  LoadingLayer.h
//  Unity-iPhone
//
//  Created by zhang fenglin on 13-2-28.
//
//

#import <Foundation/Foundation.h>

@class LoadingView;
@interface LoadingLayer : NSObject{
    UIActivityIndicatorView* spinning;
    CGRect _rect;
    UILabel* waitingLable;
    UIAlertView* alertView;
    BOOL isloading;
    BOOL isIos7;
    LoadingView* gameLoading;
}

+(LoadingLayer*)sharedInstance;

-(void)startLoading;
-(void)stopLoading;

@end
