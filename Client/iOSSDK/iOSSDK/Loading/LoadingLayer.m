//
//  LoadingLayer.m
//  Unity-iPhone
//
//  Created by zhang fenglin on 13-2-28.
//
//

#import "LoadingLayer.h"
#import "AlertContent.h"
#import "LoadingView.h"

static LoadingLayer* _instance = nil;

@implementation LoadingLayer

+(LoadingLayer*)sharedInstance{
    if (_instance == nil){
        _instance = [[self alloc] init];
    }
return _instance;
}

-(id)init{
    if (self = [super init]){
        isIos7 = [[[UIDevice currentDevice] systemVersion] compare: @"7.0" options: NSNumericSearch] != NSOrderedAscending;
        if (!isIos7)
        {
            CGRect frame = CGRectMake(0, 0, 110, 70);
            NSString* processing = [[AlertContent sharedInstance] getObjectForKey:@"processing"];
            alertView = [[UIAlertView alloc] initWithTitle:nil message:processing delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
            [alertView setFrame:frame];
            spinning = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [alertView addSubview:spinning];
            [spinning setFrame:CGRectMake(alertView.center.x, alertView.center.y, 55, 35)];
            [spinning setCenter:CGPointMake(frame.size.width + spinning.frame.size.width / 2, frame.size.height)];
        }
        else
        {
            gameLoading = [[LoadingView alloc] init];
        }
        isloading = NO;
        
    }
    return self;
}

-(void)startLoading{
    
        if (!isloading)
        {
            isloading = YES;
            if (isIos7)
            {
                [gameLoading show];
            }
            else
            {
                [alertView show];
                [spinning startAnimating];
            }
        }

    
}

-(void)stopLoading{
    if (isloading)
    {
        isloading = NO;
        if (isIos7)
        {
            [gameLoading dismiss];
        }
        else
        {
            [spinning startAnimating];
            [alertView dismissWithClickedButtonIndex:0 animated:YES];
        }
    }


}


-(void)dealloc{
    [super dealloc];
    if (isIos7)
    {
        [gameLoading release];
    }
    else
    {
        [spinning release];
        [alertView release];
    }
}

@end
