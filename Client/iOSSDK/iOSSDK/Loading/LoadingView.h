//
//  LoadingView.h
//  Unity-iPhone
//
//  Created by zhang fenglin on 13-10-10.
//
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView
{
    UIActivityIndicatorView *spinning;
}

- (void)show;
- (void)showAnimated:(BOOL)animated;
- (void)showInView:(UIView *)view animated:(BOOL)animated;
- (void)dismissAnimated:(BOOL)animated;
- (void)dismiss;
@end
