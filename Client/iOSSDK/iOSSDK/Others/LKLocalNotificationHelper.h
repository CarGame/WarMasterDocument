//
//  LocalNotificationHelper.h
//  Unity-iPhone
//
//  Created by lklk on 14-7-26.
//
//

#import <Foundation/Foundation.h>

@interface LKLocalNotificationHelper : NSObject

+ (void) removeAllLocalNotification;
+ (void) removeLocalNotificationWithID:(NSString *)messID;
+ (void) addLocalNotificationWithID:(NSString *)messID Message:(NSString *)mess FireDate:(NSDate *)fDate;
+ (BOOL) hasContainedLocalNotification:(NSString *)messID;

@end
