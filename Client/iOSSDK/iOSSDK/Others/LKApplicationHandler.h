//
//  LKApplicationHandler.h
//  Unity-iPhone
//
//  Created by lklk on 14-7-23.
//
//

#import <Foundation/Foundation.h>

@interface LKApplicationHandler : NSObject

+ (LKApplicationHandler *) sharedInstance;

- (void) application:(UIApplication *)application handleOpenURL:(NSURL *)url;
- (void) application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions;
- (void)applicationWillEnterForeground:(UIApplication *)application;
- (void)applicationDidEnterBackground:(UIApplication *)application;

@end
