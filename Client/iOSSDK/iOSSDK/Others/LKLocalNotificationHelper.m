//
//  LocalNotificationHelper.m
//  Unity-iPhone
//
//  Created by lklk on 14-7-26.
//
//

#import "LKLocalNotificationHelper.h"

#define LOCALNOTIFICATIONKEY @"LocalNotificationKey"
#define IOS8 SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")
@implementation LKLocalNotificationHelper

+ (void) removeAllLocalNotification
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

+ (void) removeLocalNotificationWithID:(NSString *)messID
{
    UIApplication *application = [UIApplication sharedApplication];
    NSArray *localNotifications = [application scheduledLocalNotifications];
    if(localNotifications!=nil && localNotifications.count >0)
    {
        for (UILocalNotification *notifcation in localNotifications)
        {
            if(notifcation.userInfo!=nil)
            {
                NSDictionary *userInfo = notifcation.userInfo;
                NSString *mID = [userInfo objectForKey:LOCALNOTIFICATIONKEY];
                if(mID!=nil)
                {
                    if([mID isEqualToString:messID])
                    {
                        [application cancelLocalNotification:notifcation];
                        return;
                    }
                }
            }
        }
    }
}

+ (void) addLocalNotificationWithID:(NSString *)messID Message:(NSString *)mess FireDate:(NSDate *)fDate
{
    
    float version =  [[[UIDevice currentDevice]systemVersion]floatValue];
    if(version>8.000000f)
    {
        NSLog(@"-----------IOS8-------------");
        UIUserNotificationSettings *uns = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert) categories: nil];
        [[UIApplication sharedApplication]registerUserNotificationSettings:uns];
        [[UIApplication sharedApplication]registerForRemoteNotifications];
        
    }
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = fDate;
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.alertBody = mess;
    notification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber+1;
    notification.repeatInterval =kCFCalendarUnitDay;
        
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:messID,LOCALNOTIFICATIONKEY, nil];
    notification.userInfo = userInfo;
        
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
    [notification release];
}

+ (BOOL) hasContainedLocalNotification:(NSString *)messID
{
    UIApplication *application = [UIApplication sharedApplication];
    NSArray *localNotifications = [application scheduledLocalNotifications];
    if(localNotifications!=nil && localNotifications.count >0)
    {
        for (UILocalNotification *notifcation in localNotifications)
        {
            if(notifcation.userInfo!=nil)
            {
                NSDictionary *userInfo = notifcation.userInfo;
                NSString *mID = [userInfo objectForKey:LOCALNOTIFICATIONKEY];
                if(mID!=nil)
                {
                    if([mID isEqualToString:messID])
                    {
                        return YES;
                    }
                }
            }
        }
    }
    
    return NO;
}

@end
