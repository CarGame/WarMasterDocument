//
//  LKApplicationHandler.m
//  Unity-iPhone
//
//  Created by lklk on 14-7-23.
//
//

#import "LKApplicationHandler.h"
#import "LKLocalNotificationHelper.h"

#define LocalNoitificationID @"LNForSprite"
#define LocalNotificationMessage @"福利嫁到，智天使限时免费召唤啦，想成为大神的王者还在等什么，快来响应天使的召唤！"

static LKApplicationHandler * handler = nil;

@implementation LKApplicationHandler

+ (LKApplicationHandler *) sharedInstance
{
    if(handler==nil)
    {
        handler = [[LKApplicationHandler alloc] init];
    }
    
    return handler;
}

- (id) init
{
    if(self = [super init])
    {
        
    }
    
    return self;
}

- (void) application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    
}

- (void) application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    //[LKLocalNotificationHelper removeLocalNotificationWithID:LocalNoitificationID];
    [LKLocalNotificationHelper removeAllLocalNotification];
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [LKLocalNotificationHelper removeLocalNotificationWithID:LocalNoitificationID];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSDate* pushDate = [formatter dateFromString:@"12:00:00"];
    
    [LKLocalNotificationHelper addLocalNotificationWithID:LocalNoitificationID Message:LocalNotificationMessage FireDate:pushDate];
    
    [formatter release];
}

@end
