//
//  URLConnection.h
//  Unity-iPhone
//
//  Created by zhang fenglin on 13-9-17.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol PaymentURLConnectDelegate
-(void)paymentFinish:(SKPaymentTransaction*) transaction data:(NSData*)data;
-(void)PaymentFailed;
@end

@interface URLConnection : NSObject <NSURLConnectionDelegate>
{
    id <PaymentURLConnectDelegate> delegate;
    SKPaymentTransaction* m_transaction;
    NSURLConnection* m_UrlConnection;
    NSMutableData* m_pReceiveData;
}

@property (nonatomic, retain) id<PaymentURLConnectDelegate> delegate;

-(void)setTarget:(id)target;
-(void)sendRequestToServer:(NSString*)username gameId:(NSString*)gameid gatewayId:(NSString*)gatewayid callbackUrl:(NSString*) callbackurl Transaction:(SKPaymentTransaction*)transaction;
@end
