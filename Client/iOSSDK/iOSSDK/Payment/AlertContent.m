//
//  AlertContent.m
//  Unity-iPhone
//
//  Created by zhang fenglin on 13-9-20.
//
//

#import "AlertContent.h"

static AlertContent* singleton = nil;
@implementation AlertContent
@synthesize msgDictionary = m_msgDictionary;
@synthesize msgAllKeys = m_msgAllKeys;

+(AlertContent*)sharedInstance
{
    return  singleton == nil ? (singleton = [[self alloc] init]) : singleton;
}

-(id)init
{
    if ((self = [super init]))
    {
        m_msgDictionary = [[NSMutableDictionary alloc] init];
        m_msgAllKeys = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)dealloc
{
    [super dealloc];
    [m_msgDictionary release];
    [m_msgAllKeys release];
}

-(void)addKey:(id<NSCopying>)key andObject:(id)object
{
    if (![m_msgAllKeys containsObject:key])
    {
        [self.msgDictionary setObject:object forKey:key];
        [self.msgAllKeys addObject:key];
    }
}

-(void)removeObjectForKey:(id)aKey
{
    [self.msgAllKeys removeObject:aKey];
    [self.msgDictionary removeObjectForKey:aKey];
}

-(NSArray*)allMsgKeys
{
    return m_msgAllKeys;
}

-(id)getObjectForKey:(id)aKey
{
    return [m_msgDictionary objectForKey:aKey];
}

@end
