//
//  AlertContent.h
//  Unity-iPhone
//
//  Created by zhang fenglin on 13-9-20.
//
//

#import <Foundation/Foundation.h>

@interface AlertContent : NSObject
{
    NSMutableDictionary* m_msgDictionary;
    NSMutableArray* m_msgAllKeys;
}
@property (nonatomic, retain) NSMutableDictionary* msgDictionary;
@property (nonatomic, retain) NSMutableArray* msgAllKeys;

+(AlertContent*)sharedInstance;
-(void)addKey:(id<NSCopying>)key andObject:(id)object;
-(void)removeObjectForKey:(id)aKey;
-(NSArray*)allMsgKeys;
-(id)getObjectForKey:(id)aKey;
@end
