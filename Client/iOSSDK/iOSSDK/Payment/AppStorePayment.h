//
//  AppStorePayment.h
//  Unity-iPhone
//
//  Created by zhang fenglin on 13-9-17.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "URLConnection.h"


@interface AppStorePayment : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver, PaymentURLConnectDelegate>
{
    NSString* userName;
    NSString* gameId;
    NSString* gatewayId;
    NSString* callbackUrl;
    NSMutableArray* m_ProductList;
    BOOL isTasking;
    BOOL isPaying;
}
@property (nonatomic, retain) NSString* userName;
@property (nonatomic, retain) NSString* gameId;
@property (nonatomic, retain) NSString* gatewayId;
@property (nonatomic, retain) NSString* callbackUrl;
+(AppStorePayment*) sharedInstance;
@end
