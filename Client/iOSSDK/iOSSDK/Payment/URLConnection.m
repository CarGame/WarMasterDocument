//
//  URLConnection.m
//  Unity-iPhone
//
//  Created by zhang fenglin on 13-9-17.
//
//

#import "URLConnection.h"

@implementation URLConnection
@synthesize delegate;

-(void)setTarget:(id)target
{
    delegate = target;
    m_pReceiveData = [[NSMutableData alloc] init];
}

-(void)dealloc
{
    [super dealloc];
    [m_pReceiveData release];
}

-(void)sendRequestToServer:(NSString *)username gameId:(NSString *)gameid gatewayId:(NSString *)gatewayid callbackUrl:(NSString *)callbackurl Transaction:(SKPaymentTransaction *)transaction
{
    m_transaction = transaction;
    NSString* transactionReceipt =  [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
   // NSLog(@"transactionReceipt %@",transactionReceipt);
    NSData* transData = [transactionReceipt dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    transactionReceipt = [transData base64Encoding];
    //NSLog(@"transactionReceipt64 %@",transactionReceipt);
    NSString* json = [NSString stringWithFormat:@"{\"userName\":\"%@\",\"gameId\":\"%@\",\"gatewayId\":\"%@\",\"productId\":\"%@\",\"transactionReceipt\":\"%@\",\"time\":\"%@\"}",username,gameid, gatewayid,m_transaction.payment.productIdentifier,transactionReceipt,[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
    
//    NSLog(@"request data : %@", json);
    NSData* jsonData = [json dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSString* base64string = [NSString stringWithFormat:@"data=\"%@\"",[jsonData base64Encoding]];
//    NSLog(@"base data is : %@", base64string);
    NSData* postData = [base64string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
//    NSString* postLength = [NSString stringWithFormat:@"%d", [postData length]];
    NSURL* url = [NSURL URLWithString:callbackurl];
    NSMutableURLRequest* request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [request setTimeoutInterval:5];
//    m_UrlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    m_UrlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
}

#pragma mark 代理方法
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//    NSLog(@"didReceiveResponse");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
//    NSLog(@"didReceiveData");
    [m_pReceiveData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate PaymentFailed];
//    NSLog(@"didFailWithError");
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
//    NSLog(@"connectionDidFinishLoading %@", [[NSString alloc] initWithData:m_pReceiveData encoding:NSUTF8StringEncoding]);
    
    [delegate paymentFinish:m_transaction data:m_pReceiveData];
}

@end
